(deftemplate libro
  (slot titulo
    (type STRING)
    (default "Desconocido")
  )
  (slot autor
    (type STRING)
    (default "Desconocido")
  )
  (slot gestion
    (type INTEGER)
    (default 1990)
    (range 1600 2030)
  )
)