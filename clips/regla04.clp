(deffacts familia  "Nucleo familiar"
    (padre-de Javier Fernando)
    (madre-de Littzy Fernando)
    (padre-de Javier Jorge)
    (madre-de Littzy Jorge)
        

    (mujer Littzy)
    (hombre Javier)

    (hombre Fernando)
    
    (hombre Jorge)

    (esposa-de Littzy Javier)
    (marido-de Javier Littzy)
)

(defrule ddddd    
    (mujer ?l)
    (hombre ?j)
    (padre-de ?j ?f)
    (marido-de ?j ?l)
    =>
    (printout t  ?f "Es hijo de" ?j  crlf) 
)
