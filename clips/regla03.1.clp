;Realizar un SE.
;para obtener la potencia de un numero x elevado a y
;____________________________________________________
;HECHOS
(deffacts datos
   ;(numero x y) se entiende como: x elevado a y
    (numero 4 2)
    (numero 3 3)    
)

;REGLAS
(defrule Pontencia
    (numero ?x ?y) 
    => 
    (printout t  (** ?x ?y)  crlf) 
    ; t = imprimir por linea
    ; crlf = retorno de carro.
    ; La evaluación de esta expresión 
    ;en CLIPS es en notación PREFIJA
) 