(defrule Suma 
    (numeros ?x ?y) 
    => 
    (assert (resultado_suma (+ ?x ?y)))
) 
(deffacts datos
    (numeros 4 5)
    (numeros 3 8)
)