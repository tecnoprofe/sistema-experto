(deftemplate docente "datos de un docente"
    (slot ci)
    (slot nombre (type STRING))
    (slot edad (type INTEGER) (default 0))
    (multislot direccion)
)
(deffacts docentes
    (docente (ci 1234567) (nombre "Gabriela") (edad 18) (direccion avenida busch))
    (docente (ci 1234587) (nombre "Roger") (edad 20) (direccion avenida San Martin))
    (docente (ci 5558884) (nombre "Camilo") (edad 25))
    (docente (nombre "Ana") (edad 24))
)