(defrule nombreJuan
   (nombre Juan)
=>
   (printout t "Tu nombre de pila es Juan" crlf)
) 
(defrule nombreyapellidos
   (nombre Juan)
   (apellido-1 Perez)
   (apellido-2 Lopez)
=>
   (printout t "Te llamas Juan Perez Lopez" crlf)
)