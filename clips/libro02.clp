(deftemplate libro
  (slot titulo
    (type STRING)
    (default "Desconocido")
  )
  (slot autor
    (type STRING)
    (default "Desconocido")
  )
  (slot gestion
    (type INTEGER)
    (default 1990)
    (range 1600 2030)
  )
)
(deffacts libros  "agregar muchos libros"
  (libro (titulo "C++") (autor "Stephen Prata") (gestion 2002))
  (libro (titulo "ANSI C") (autor "Stephen Prata") (gestion 2000))  
)