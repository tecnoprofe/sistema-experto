(deffacts semaforo
    (semaforo verde)     ;hecho de tipo ordenado y el tipo es simbolo
    (semaforo naranja)    
    (semaforo rojo)    
)

(defrule semaforo-R
    (semaforo rojo)
    =>
    (assert(PARAR))
)