(deftemplate libro
  (multislot titulo
    (type SYMBOL)
    (default Desconocido)
  )
  (slot autor
    (type STRING)
    (default "Desconocido")
  )
  (slot dato
    (type INTEGER)
    (default 1990)
    (range 1600 2027)
  )
)

(deffacts libros "Agregar libros"
  (libro (titulo Clisp libro) (autor "Giarratano") (dato 2001))
  (libro (titulo Common Lisp) (autor "Someone") (dato 2005))
  (libro (titulo C++) (autor "Stephen Prata") (dato 2002))
  (libro (titulo ANSI C) (autor "Stephen Prata") (dato 2000))
  (libro (titulo El senior de los anillos La comunidad del anillo) (autor "Tolkien"))
  (libro (titulo El senior de los anillos Las dos torres) (autor "Tolkien"))
  (libro (titulo El senior de los anillos El regreso del rey) (autor "Tolkien"))
)

(defrule rem-lord "remove all libros with lord in the titulo"
  (remove-lords 1)
  ?what <- (libro (titulo $?titulo_beg senior $?titulo_end) (autor ?aut) (dato ?dato))
  =>
    (printout t "Removing " $?titulo_beg " senior " $?titulo_end " libro, autor is " ?aut " and the dato is " 
?dato crlf)
    (retract ?what))
